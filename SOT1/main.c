/*
* @file : <nombre del archivo: por ejemplo, “tarea01.c”>
* @author : - Aline Cobarrubias
            - Cristóbal Roldán
* @date : 27/10/2021
* @brief : Código para tarea 01 parte 2 en ELO 321, semestre 2021-2
*
* Compilar con -> gcc main.c -o main -std=c99 -pthread
* Luego ejecutar acompañado del argumento correspondiente.
*/


//---------- Includes ---------------------
#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>


//------------ Defines -------------------
#define LENGTH 46   // Luego de esto ocurre un overflow

// ------ Variables Globales -------------
int Fib_Array[LENGTH];
int input=0;

// -------- Fn Prototipos -----------------
void* get_Fibonacci_series(void* val);

//-----------------------------------------

/* Main: el proceso incial, posee el control sobre el valor para asegurar
         que sea un entero positivo, luego llama al threat y espera.
         Finalmente imprime la secuencia de fibonacci
*/

int main(int argc, char *argv[]) {
    input = atoi(argv[1]); // convierte el argumento a entero.

    if(input>=0 && input<= LENGTH){
        pthread_t tid;
        pthread_create(&tid, NULL , get_Fibonacci_series, &input);
        //printf("Proceso hijo creado, esperando ...\n");

        pthread_join(tid, NULL);  // espera al hijo.

        printf("\nLa sucesión de Fibonacci es : {");  //Proceso principal

        for (int i=0; i<= input ; i++){
            printf("%d, ",Fib_Array[i]);
        }
        printf("}\n\n");
        return 0;
        }

        
    else{ //Error, numero excede máximo o es negativo.
        return 1;
    }
}

// -------------- Funciónes -----------------------------------

/* get_Fibonacci_series es llamada por el hilo, y guarla la sucesión de fibonaci en
   el arreglo Fib_Array[]*/

void* get_Fibonacci_series(void* val){ //Ejecutado por el thread.

    int max = *((int*)val);
                                            // acá podría ir un mutex, pero es solo un hilo, y el padre tiene que esperar
    Fib_Array[0]=0;  // F0 = 0
    Fib_Array[1]=1;
    for (int i=2; i <= max; i++){
        Fib_Array[i]= Fib_Array[i - 1] + Fib_Array[i - 2];
       // printf("N_%d :  %d \n", i, Fib_Array[i]);
    }
    printf("Soy el thread N= %d, la operación fué ejecutada con éxito! \n", pthread_self() );


    pthread_exit(0); // finaliza el hilo
}
